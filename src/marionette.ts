import * as THREE from 'three'
import * as SkeletonUtils from 'three/examples/jsm/utils/SkeletonUtils'
import * as BonesConfig from './bones_config'
import { SkeletonSerializer } from './skeleton_serializer'


export const MODEL_NAME_PREFIX = 'model_'

const MIN_POS = new THREE.Vector3(-1.8, -1.8, -1.8)
const MAX_POS = new THREE.Vector3( 1.8,  1.8,  1.8)


type colored_segment = [ THREE.Vector3, THREE.Vector3, THREE.ColorRepresentation ]


export class Marionette {
    name: string
	default_pose: string
	skeleton: THREE.Skeleton
	root_bone: THREE.Bone
	focused_bone_child: THREE.Bone
	model: THREE.Group
	doing_something: boolean
	serializer: SkeletonSerializer
	bones_world_pos: { [id: string] : THREE.Vector3 }

	constructor(name: string, default_pose: string) {
        this.name = name
		this.default_pose = default_pose

		this.skeleton = new THREE.Skeleton([])
		this.root_bone = new THREE.Bone()
		this.focused_bone_child = new THREE.Bone()
		this.model = new THREE.Group()
		this.doing_something = false
		this.serializer = new SkeletonSerializer(MIN_POS, MAX_POS)
		this.bones_world_pos = {}
	}

	#updateRootBone() {
		this.root_bone = <THREE.Bone> this.model.children.find(child => child instanceof THREE.Bone)
	}

	get focused_bone(): THREE.Bone {
		if (this.focused_bone_child.parent) {
			return <THREE.Bone> this.focused_bone_child.parent
		} else {
			throw new BonesConfig.BoneNotFoundError(`${ this.focused_bone_child }'s parent`)
		}
	}

	get root_bone_world_pos(): THREE.Vector3 {
		return this.bones_world_pos[this.root_bone.name]
	}

	get focused_bone_world_pos(): THREE.Vector3 {
		return this.bones_world_pos[this.focused_bone.name]
	}

	get focused_bone_child_world_pos(): THREE.Vector3 {
		return this.bones_world_pos[this.focused_bone_child.name]
	}

	onModelLoaded(model: THREE.Group) {
		this.model = <THREE.Group> SkeletonUtils.clone(model)
		this.model.name = MODEL_NAME_PREFIX + this.name

		this.#updateRootBone()
		this.root_bone.traverse(bone => {
			if (bone.parent && bone.name != bone.parent.name) {
				this.skeleton.bones.push(<THREE.Bone> bone)
				this.bones_world_pos[bone.name] = new THREE.Vector3()
			}
		})

		if ( this.skeleton.bones.length == 0 ) {
			throw(`Skeleton not found in the model.`)
		}

		// this.resetPose()
		this.updateBonesWorldPos()
	}

	updateBonesWorldPos() {
		// need optimization: iterate from a specific bone instead over all bones
		this.skeleton.bones.forEach(bone => {
			bone.getWorldPosition(this.bones_world_pos[bone.name])
		})
	}

	resetPose() {
		this.loadFromString(this.default_pose)
	}

	loadFromString(str: string) {
		this.serializer.fromString(str)

		const bones_rotation = this.serializer.getBonesRotation()
		for (const [bone_name, bone_rotation] of Object.entries(bones_rotation)) {
			const bone = this.skeleton.getBoneByName(bone_name)
			if (bone) {
				bone.rotation.copy(bone_rotation)
			} else {
				throw new BonesConfig.BoneNotFoundError(bone_name)
			}
		}

		this.model.position.copy(this.serializer.getModelPosition())
	}

	toString(): string {
		return this.serializer.toString()
	}

	translateModel(axis: THREE.Vector3, distance: number) {
		this.model.translateOnAxis(axis, distance)
		this.model.position.clamp(MIN_POS, MAX_POS)
		this.updateBonesWorldPos()
	}

	rotateModel(axis: THREE.Vector3, angle: number) {
		this.root_bone.rotateOnWorldAxis(axis, angle)
		this.updateBonesWorldPos()
	}

	rotateFocusedBone(axis: THREE.Vector3, angle: number) {
		this.focused_bone.rotateOnWorldAxis(axis, angle)
		this.updateBonesWorldPos()
	}

	updateFocusedBone(target: THREE.Vector3): colored_segment[] {
		let min_height = Infinity
		let closest_bone = new THREE.Bone()
		const segments: { [id: string] : colored_segment } = {}

		this.skeleton.bones.forEach(bone => {
			if (bone.parent && bone.name != this.root_bone.name) {
				const start = this.bones_world_pos[bone.parent.name]
				const end = this.bones_world_pos[bone.name]

				const se = start.clone().sub(end)
				const st = start.clone().sub(target)
				
				const dot = se.dot(st)
				if (dot > 0) {
					segments[bone.name] = [ start, end, 0x888888 ]
					const alpha = se.angleTo(st)
					const st_len = st.length()
					const height = Math.sin(alpha) * st_len
					if (height < min_height) {
						min_height = height
						closest_bone = bone
					}
					// console.log('bone candidate:', bone.name, dot, height)
				}
			}
		})
		this.focused_bone_child = <THREE.Bone> closest_bone
		segments[closest_bone.name][2] = 0xffffff
		return Object.values(segments)
	}

	updateFocusedBoneOld(point: THREE.Vector3): colored_segment[] {
		const bone_world_position = new THREE.Vector3()
		let closest_bone = new THREE.Bone
		let closest_distance = Infinity
		let closest_point = new THREE.Vector3()

		BonesConfig.forEachEnabledBone(this.skeleton, bone => {
			bone.getWorldPosition(bone_world_position)
			const distance = (bone_world_position.clone().sub(point)).length()
			if (distance < closest_distance) {
				closest_bone = bone
				closest_distance = distance
				closest_point.copy(bone_world_position)
			}
		})

		this.focused_bone_child = closest_bone
		return [ [point, closest_point, 0xffffff] ]
	}

	roundPosition() {
		this.serializer.loadModelPosition(this.model.position)
		this.model.position.copy(this.serializer.getModelPosition())
	}

	roundBone(bone: THREE.Bone) {
		try {
			this.serializer.loadBoneRotation(bone)
			bone.rotation.copy(this.serializer.getBoneRotation(bone.name))
		} catch(ReferenceError) {
			console.warn(`Bone ${ bone.name } is not listed in bones config, passing...`)
		}
	}

}
