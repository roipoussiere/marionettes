import * as THREE from 'three'


type colored_segment = [ THREE.Vector3, THREE.Vector3, THREE.ColorRepresentation ]
type colored_dot = [ THREE.Vector3, THREE.ColorRepresentation ]
type axe = [ THREE.Vector3, THREE.Quaternion ]


const MATERIAL_SLICE_SMALL = new THREE.MeshBasicMaterial({
	color: 0xffffff,
	side: THREE.DoubleSide,
	opacity: 0.6,
	transparent: true
})

const MATERIAL_SLICE_BIG = new THREE.MeshBasicMaterial({
	color: 0xffffff,
	side: THREE.DoubleSide,
	depthTest: false,
	opacity: 0.1,
	transparent: true
})


export class Helpers {
	group: THREE.Group

	plane: THREE.PlaneHelper
	slice: THREE.Group
	segments: THREE.Group
	dots: THREE.Group
	arrows: THREE.Group
	axes: THREE.Group
	cylinder: THREE.Group

	constructor() {
		this.group = new THREE.Group()

		this.plane = new THREE.PlaneHelper(new THREE.Plane(), 3, 0xffff00)
		this.slice = this.#buildSlice()
		this.segments = new THREE.Group()
		this.dots = new THREE.Group()
		this.arrows = new THREE.Group()
		this.axes = new THREE.Group()
		this.cylinder = this.#buildCylinder()
	}

	init(enable: boolean) {
		this.group.visible = enable

		this.group.name = 'helpers'
		this.plane.name = 'plane_helper'
		this.slice.name = 'slice_helper'
		this.segments.name = 'segments_helper'
		this.dots.name = 'dots_helper'
		this.arrows.name = 'arrows_helper'
		this.axes.name = 'axes_helper'

		this.group.add(
			this.plane,
			this.slice,
			this.segments,
			this.dots,
			this.arrows,
			this.axes,
			this.cylinder
		)
	}

	get is_enabled(): boolean {
		return this.group.visible
	}

	set is_enabled(is_visible: boolean) {
		this.group.visible = is_visible
	}

	#buildSlice(): THREE.Group {
		const small_circle = new THREE.Mesh(new THREE.CircleGeometry(0.5, 16), MATERIAL_SLICE_SMALL)
		const big_circle = new THREE.Mesh(new THREE.CircleGeometry(1, 16), MATERIAL_SLICE_BIG)
		return new THREE.Group().add(small_circle, big_circle)
	}

	#buildCylinder(): THREE.Group {
		const geometry = new THREE.CylinderGeometry(0.01, 0.01, 0.5)
		const material = new THREE.MeshBasicMaterial({ color: 0x555555 })
		const cylinder = new THREE.Mesh(geometry, material)
			.rotateX(Math.PI / 2)
			.translateY(0.25)
		return new THREE.Group().add(cylinder)
	}

	updateSlice(position: THREE.Vector3, direction: THREE.Vector3, radius: number) {
		this.slice.lookAt(position.clone().add(direction))
		this.slice.position.copy(position)
		this.slice.scale.setScalar(radius)
	}

	addSegments(segments: colored_segment[]) {
		if (this.is_enabled) {
			segments.forEach(segment => {
				const material = new THREE.LineBasicMaterial({
					color: segment[2],
					depthTest: false,
					transparent: true
				})

				const geometry = new THREE.BufferGeometry().setFromPoints([ segment[0], segment[1] ])

				const object = new THREE.Line(geometry, material)

				this.segments.add(object)
			})
		}
	}

	addDots(dots: colored_dot[]) {
		if (this.is_enabled) {
			dots.forEach(dot => {
				const material = new THREE.MeshBasicMaterial({
					color: dot[1],
					depthTest: false,
					opacity: 0.5,
					transparent: true
				})
			
				const geometry = new THREE.SphereGeometry(0.02, 6, 4)

				const object = new THREE.Mesh(geometry, material)
				object.position.copy(dot[0])
			
				this.dots.add(object)
			})
		}
	}

	updateArrow(position: THREE.Vector3, direction: THREE.Vector3) {
		if (this.is_enabled) {
			this.arrows.clear()
			this.arrows.add(new THREE.ArrowHelper(direction, position, 1, 0xffffff))
			this.arrows.add(new THREE.ArrowHelper(direction.negate(), position, 1, 0xffffff))
		}
	}

	addAxes(axes: axe[]) {
		if (this.is_enabled) {
			axes.forEach(axe => {
				const axes_helper = new THREE.AxesHelper(0.5)
				axes_helper.position.copy(axe[0])
				axes_helper.rotation.setFromQuaternion(axe[1])
				this.axes.add(axes_helper)
			})
		}
	}

	replaceSegments(segments: colored_segment[]) {
		this.segments.clear()
		this.addSegments(segments)
	}

	replaceDots(dots: colored_dot[]) {
		this.dots.clear()
		this.addDots(dots)
	}

	replaceAxes(axes: axe[]) {
		this.axes.clear()
		this.addAxes(axes)
	}
}
