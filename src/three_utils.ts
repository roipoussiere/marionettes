import * as THREE from 'three'


const DEFAULT_COLOR = new THREE.Color(0xff3399)


export function drawCylinder(start: THREE.Vector3, end: THREE.Vector3, color = DEFAULT_COLOR) {
	const line = new THREE.Object3D()
	const direction = end.clone().sub(start)
	const cylinder_geometry = new THREE.CylinderGeometry(
		0.001,
		0.001,
		direction.length(),
		6
	)
	const cylinder_material = new THREE.MeshBasicMaterial({color})
	const cylinder = new THREE.Mesh(cylinder_geometry, cylinder_material)

	cylinder.translateZ(direction.length() * 0.5)
	cylinder.rotateX(Math.PI / 2)

	line.name = "LineHint"
	line.add(cylinder)
	line.lookAt(direction)
	line.position.add(start)

	return line
}

export function dumpScene(scene: THREE.Scene) {
    scene.traverse( obj => {
        let s = '+---';
        let obj2 = obj;
        while ( obj2 != scene ) {
            s = '\t' + s;
            if (obj2.parent != null) {
                obj2 = obj2.parent;
            } else {
                break
            }
        }
        console.log( s + obj.name + ' <' + obj.type + '>' );
    });
}

export function dumpBone(bone: THREE.Bone) {
	const rotation = new THREE.Vector3().setFromEuler(bone.rotation)
	console.log(
		`${ bone.name }: `
		+ `(${ Math.round(rotation.x * THREE.MathUtils.RAD2DEG) }, `
		+  `${ Math.round(rotation.y * THREE.MathUtils.RAD2DEG) }, `
		+  `${ Math.round(rotation.z * THREE.MathUtils.RAD2DEG) })`
	)
}